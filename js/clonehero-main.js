function Note(type, position, track){
    this.type = type;
    this.position = position;
    this.belongsTo = track;
}
function Chart(noteCount, length, notes){
    this.noteCount = noteCount;
    this.length = length;
    this.notes = notes;
}
function Track(name, chart){
    this.name = name;
    this.chart = chart;
}